import discord
from discord.ext import commands
import json
import os
from dotenv import load_dotenv

intents = discord.Intents.all()
bot = commands.Bot(command_prefix='!', intents=intents)

current_room = None
player_inventory = []

load_dotenv()
TOKEN = os.getenv("DISCORD_TOKEN")

with open('./static/items.json', 'r') as items_file:
    item_actions = json.load(items_file)

with open('./static/game_world.json', 'r') as game_world_file:
    game_world = json.load(game_world_file)        

@bot.event
async def on_ready():
    print(f'Logged in as {bot.user.name}')        

@bot.command(name='adventure', help='Start an adventure')
async def start_adventure(ctx):
    global current_room
    current_room = 'starting_room' 
    room_description = show_room(current_room)
    await ctx.send(room_description)

@bot.command(name='pick', help='Pick up an item')
async def pick_item(ctx, item_name):
    global current_room
    if current_room in game_world:
        current_room_data = game_world[current_room]
    if item_name in current_room_data['items']:
        current_room_data['items'].remove(item_name)
        player_inventory.append(item_name)
        await ctx.send(f'You picked up the {item_name}.')
    else:
        await ctx.send(f'There is no {item_name} here to pick up.')

@bot.command(name='use', help='Use an item')
async def use_item(ctx, item_name):
    if item_name in player_inventory:
        if item_name in item_actions:
            action = item_actions[item_name]['use']
            if action == 'unlock_door':
                if game_world['locked_door']['locked']:
                    game_world['locked_door']['locked'] = False
                    await ctx.send('You used the key to unlock the door!')
                else:
                    await ctx.send('The door is already unlocked.')
        else:
            await ctx.send('You cannot use this item.')
    else:
        await ctx.send('You do not have that item in your inventory.')
    
@bot.command(name='move', help='Walk to the room specified by direction: left, right, front, back.')
async def move(ctx, direction):
    global current_room, game_world
    if current_room in game_world:
        current_room_data = game_world[current_room]
        if direction in current_room_data['exits']:
            next_room_name = current_room_data['exits'][direction]
            if next_room_name in game_world:
                current_room = next_room_name  
                room_description = show_room(current_room)  
                await ctx.send(room_description)
            else:
                await ctx.send('There is no room in that direction.')
        else:
            await ctx.send('You cannot go that way.')
    else:
        await ctx.send('Invalid room name.')    
        
def show_room(room_name):
    if room_name in game_world:
        room_data = game_world[room_name]
        description = room_data['description']
        return description
    else:
        return 'Invalid room name.'     

bot.run(TOKEN)
